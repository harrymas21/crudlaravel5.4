## About:
	-CRUD application with laravel 5.4.
	-Uses Resource Controllers

## Used:
	-Laravel 5.4
	-Bootstrap
	-My SQL

## Installation:
	-Modify the db:username:password variables and migrate database.
	

## License
[MIT license]: (http://opensource.org/licenses/MIT)
